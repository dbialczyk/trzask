package main.java.com.company;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class InitTest {
    WebDriver browser;

    @Before
    public void setup () {
        System.setProperty("webdriver.chrome.driver", "./chromedriver");
        browser = new ChromeDriver();
    }

    @Test
    public void site_header_is_on_home_page() throws InterruptedException {
        browser.get("https://wearmedicine.com/ona");
        WebElement przycisk = browser.findElement(By.xpath("/html/body/div[2]/main/div[1]/div/div[2]/div/div/a"));
        przycisk.click();
        Thread.sleep(3000);
        WebElement napis = browser.findElement(By.xpath("/html/body/div[2]/main/div[1]/h3"));
        System.out.println("napis: " + napis.getText());
        assertTrue((napis.isDisplayed()));
    }

    @After
    public void finish() {
        browser.close();

    }
}